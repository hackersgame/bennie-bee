import bge

# Punt on OOP
class a_classy_start( bge.types.KX_PythonComponent):
    pass

player_stat = {"Life":100}
a_classy_start.args = player_stat

def start(oops, args):
    print("Starting...")

a_classy_start.start = start


def update(oops):
    print("Tick ...")

a_classy_start.update = update

print(dir(a_classy_start))