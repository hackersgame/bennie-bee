#!/usr/bin/python3
import socket

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
IP = "10.250.10.133"
port = 6666


def connect_to_server():
    client.connect((IP, port))
    #return client.recv(4096).decode()

def send_to_server(data):
    client.send(str.encode(data))
    return client.recv(4096).decode()
    
connect_to_server()

while True:
    send_to_server("Yo")
