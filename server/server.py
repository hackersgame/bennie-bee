#!/usr/bin/python3
import socket
from _thread import *

def getIP():
    import socket
    #Thanks: https://stackoverflow.com/a/1267524/5282272
    return (([ip for ip in socket.gethostbyname_ex(socket.gethostname())[2] if not ip.startswith("127.")] or [[(s.connect(("8.8.8.8", 53)), s.getsockname()[0], s.close()) for s in [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)]][0][1]]) + ["no IP found"])[0]

port = 6666
IP = getIP()
print(IP)
print("Hi")
#server = socket.socket(socket.AF_PACKET, socket.SOCK_RAW)
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
#socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

try:
    server.bind((IP, port))
except Exception as error:
    print(error)
    exit(1)


server.listen(1) 
print("Server Started")


def threaded_client(conn, player_index):
    conn.send(str.encode("Connected"))
    reply = ""
    while True:
        try:
            data = conn.recv(2048)
            reply = data.decode("utf-8")

            if not data:
                print("Disconnected")
                break
            else:
                print("Received: ", reply)
                print("Sending : ", reply)

            conn.sendall(str.encode(reply))
        except:
            break

    print("Lost connection")
    conn.close()
    
player_index = 0   
while True:
    conn, addr = server.accept()
    start_new_thread(threaded_client, (conn, player_index))
    player_index = player_index + 1
    #while True:
    #    data = conn.recv(4096)
    #    print(data)
    #conn, addr = socket.accept()
    #print("Connected to:", addr)
    
    #start_new_thread(threaded_client, (conn,))
